// import logo from './logo.svg';
import './App.css';
import { useState, useRef, useEffect } from 'react';

function App() {
  //state
  const [todos, setTodos] = useState([])

  //binding
  const todoText = useRef()

  //lifecycle
  useEffect(() => {
    const existingTodos = localStorage.getItem('todos')
    setTodos(existingTodos ? JSON.parse(existingTodos) : [])
  }, [])

  const addTodo = (event) => {
    event.preventDefault()
    const next = [...todos, todoText.current.value]
    setTodos(next)
    localStorage.setItem('todos', JSON.stringify(next))
  }

  return (
    <div>
      <h1>Todo List</h1>
      <ul>
        {todos.map(todo => (<li key={todo}>{todo}</li>))}
      </ul>
      <form onSubmit={addTodo}>
        <input type="text" ref={todoText}/>
        <input type="submit" value="Add Todo"/>
      </form>
    </div>
  )

}

export default App;
